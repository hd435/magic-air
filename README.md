# Deep ANPR

# NOTE: only contains codes, training and test dataset not uploaded due to large size

This project is based on and modified from https://github.com/matthewearl/deep-anpr. It uses deeping learning and convolutional neural network to train a model for number plate detection.


Usage is as follows:

1. `./extractbgs.py SUN397.tar.gz`: Extract ~3GB of background images from the [SUN database](http://groups.csail.mit.edu/vision/SUN/)
   into `bgs/`. (`bgs/` must not already exist.) The tar file (36GB) can be [downloaded here](http://vision.princeton.edu/projects/2010/SUN/SUN397.tar.gz).
   This step may take a while as it will extract 108,634 images.

2. Download `UKNumberPlate.ttf` into the
    `fonts/` directory, which can be found from (http://www.dafont.com/uk-number-plate.font).
   (`UKNumberPlate.ttf` has been downloaded, you can skip this step)


3. `./gen.py 1000`: Generate 1000 test set images in `test/`. (`test/` must not
    already exist.) For better model, generate more test images, eg `./gen.py 10000`


4. `./train.py`: Train the model. A GPU is recommended for this step. It will
   take around 100,000 batches to converge. When you're satisfied that the
   network has learned enough press `Ctrl+C` and the process will write the
   weights to `weights.npz` and return.
   # a trained model `weights.npz` is provided 

5. `./select_frame.py`: Select, and crop every 7th frame to reduce number & size of images 
   needed to be analysed. Save videos into video folder. Then change `video_names` and `crop`
   list accordingly.

6. `./detect.py in.jpg weights.npz out.jpg`: Detect number plates in a single image.
      or
7. `./detect_batch.py weights.npz`: Detect images from output folder of `select_frame.py` 
    #raw_csv folder must exist

The project has the following dependencies:

* TensorFlow
* OpenCV
* NumPy

* cuda 11.2 (https://developer.nvidia.com/cuda-11.2.0-download-archive)
* cuDNN v8.1.1 (Feburary 26th, 2021), for CUDA 11.0,11.1 and 11.2 (https://developer.nvidia.com/rdp/cudnn-archive)
#cuda and cuDNN will enable use of GPU, which is much much faster than CPU, if you want to analyse large number of imgs and has a nice GPU, it is strongly recommended



